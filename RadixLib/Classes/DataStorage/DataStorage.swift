//
//  DataAccess.swift
//  RadixLib
//
//  Created by Anderson Lucas C. Ramos on 07/02/17.
//  Copyright © 2017 Radix Eng & Soft. All rights reserved.
//

import Foundation

public struct DataStorages {
	public static let userDefaults: DataStorageProtocol.Type = DaoUserDefaults.self
	public static let keychain: DataStorageProtocol.Type = DaoKeychain.self
	public static let jsonFile: DataStorageProtocol.Type = DaoJsonFile.self
}

public class DataStorage : Logger {
	fileprivate var object: DataStorageProtocol?
	
	public var rawStorage: DataStorageProtocol? {
		return self.object
	}
	
	public init(withDaoType type: DataStorageProtocol.Type) {
		self.object = type.instance
	}
	
	public func save<T>(object: T?, forKey key: String) {
		self.object?.save(object, forKey: key)
	}
	
	public func load<T>(objectForKey key: String) -> T? {
		let value = self.object?.load(objectForKey: key) as? T
		return value
	}
	
	public func delete(objectForKey key: String) {
		self.object?.delete(objectForKey: key)
	}
	
	public func synchronize() {
		self.object?.synchronize()
	}
	
	public subscript(key: String) -> Any? {
		get {
			return self.load(objectForKey: key)
		}
		set {
			if newValue != nil {
				self.save(object: newValue, forKey: key)
			} else {
				self.delete(objectForKey: key)
			}
		}
	}
}
