//
//  DaoUserDefaults.swift
//  RadixLib
//
//  Created by Anderson Lucas C. Ramos on 07/02/17.
//  Copyright © 2017 Radix Eng & Soft. All rights reserved.
//

import Foundation

public class DaoUserDefaults: DataStorageProtocol {
	public static var instance: DataStorageProtocol = DaoUserDefaults()
	
	fileprivate let defaults = UserDefaults.standard
	
	fileprivate init() {
		
	}
	
	public func save(_ object: Any?, forKey key: String) {
		self.defaults.set(object, forKey: key)
	}
	
	public func load(objectForKey key: String) -> Any? {
		return self.defaults.object(forKey: key)
	}
	
	public func delete(objectForKey key: String) {
		self.defaults.removeObject(forKey: key)
	}
	
	public func synchronize() {
		self.defaults.synchronize()
	}
}
