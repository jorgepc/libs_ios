//
//  DaoKeychain.swift
//  RadixLib
//
//  Created by Anderson Lucas C. Ramos on 08/02/17.
//  Copyright © 2017 Radix Eng & Soft. All rights reserved.
//

import Foundation
import KeychainAccess

internal struct DaoExceptionName {
	static let objectTypeNotSupported: NSExceptionName = NSExceptionName("objectTypeNotSupported")
}

internal class DaoKeychainException : NSException {
	init() {
		super.init(name: DaoExceptionName.objectTypeNotSupported, reason: "Cannot store this object type. Please, use Int, Float or Data types.", userInfo: nil)
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
}

public class DaoKeychain: DataStorageProtocol {
	public static var instance: DataStorageProtocol = DaoKeychain()
	
	fileprivate let keychain = Keychain()
	
	fileprivate init() {
		
	}
	
	public func save(_ object: Any?, forKey key: String) {
		if object is String {
			self.keychain[key] = object as? String
		} else if object is Int {
			self.keychain[key] = String(object as! Int)
		} else if object is Float {
			self.keychain[key] = String(object as! Float)
		} else if object is CGFloat {
			self.keychain[key] = String(object as! Float)
		} else if object is Int64 {
			self.keychain[key] = String(object as! Int64)
		} else if object is Int16 {
			self.keychain[key] = String(object as! Int16)
		} else if object is Double {
			self.keychain[key] = String(object as! Double)
		} else if object is Data {
			self.keychain[data: key] = (object as! Data)
		} else {
			objc_exception_throw(DaoKeychainException())
		}
		
	}

	public func load(objectForKey key: String) -> Any? {
		return self.keychain[key]
	}
	
	public func load(dataForKey key: String) -> Data? {
		return self.keychain[data: key]
	}
	
	public func delete(objectForKey key: String) {
		try? self.keychain.remove(key)
	}
	
	public func synchronize() {
		// Do nothing for keychain
	}
}
