//
//  BaseRequest.swift
//  RadixLib
//
//  Created by Anderson Lucas C. Ramos on 06/02/17.
//  Copyright © 2017 Radix Eng & Soft. All rights reserved.
//

import Foundation
import Swift_Json

open class BaseRequest : Logger {
	fileprivate var session: URLSession!
	fileprivate var sessionTask: URLSessionDataTask!
	
	open fileprivate(set) var isRunning: Bool = false
	
	public init() {
		self.session = URLSession(configuration: URLSessionConfiguration.default)
	}
	
	open func getHeaderFields() -> [String: String]? {
		return nil
	}
	
	open func getQueryParams() -> [String: String]? {
		return nil
	}
	
	open func getStringBodyContent() -> String? {
		return nil
	}
	
	open func getJsonBodyObject<T : NSObject>() -> T? {
		return nil
	}
	
	open func getJsonConfig() -> JsonConfig? {
		return nil
	}
	
	fileprivate func setupHeaders(_ request: NSMutableURLRequest, _ headers: [String: String]) {
		log("setup headers: \(headers)")
		for header in headers {
			request.addValue(header.value, forHTTPHeaderField: header.key)
		}
	}
	
	fileprivate func setupQueryParams(_ url: String, _ params: [String: String]) -> String {
		if params.count == 0 { return "" } // grant that there is at least one parameter
		
		var queryString = url.contains("?") ? "&" : "?"
		for param in params {
			guard let scapedKey = param.key.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) else { continue }
			guard let scapedValue = param.value.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) else { continue }
			queryString += "\(scapedKey)=\(scapedValue)&"
		}
		queryString.remove(at: queryString.index(before: queryString.endIndex))
		return queryString
	}
	
	fileprivate func setupStreamBodyParams(_ request: NSMutableURLRequest, _ params: [String: String]) {
		var stringParams = self.setupQueryParams("?", params)
		stringParams.remove(at: stringParams.index(stringParams.startIndex, offsetBy: 0))
		request.httpBody = stringParams.data(using: .utf8)
	}
	
	fileprivate func setupStreamStringBody(_ request: NSMutableURLRequest, _ content: String) {
		request.httpBody = content.data(using: .utf8)
	}
	
	fileprivate func setupStreamJsonBody(_ request: NSMutableURLRequest, _ object: NSObject) {
		request.addValue("application/json", forHTTPHeaderField: "Content-Type")
		
		let writer = JsonWriter()
		guard let jsonBodyString = writer.write(anyObject: object, withConfig: self.getJsonConfig()) else {
			log("Could not set the URLRequest.httpBody, the JsonWriter returned nil.")
			return
		}
		request.httpBody = jsonBodyString.data(using: .utf8)
	}
	
	fileprivate func generateQueryRequest(_ request: NSMutableURLRequest) -> NSMutableURLRequest {
		guard let params = self.getQueryParams() else { return request }
		guard var urlString = request.url?.absoluteString else { return request }
		urlString += self.setupQueryParams(urlString, params)
		request.url = URL(string: urlString)
		return request
	}
	
	fileprivate func generateStreamRequest(_ request: NSMutableURLRequest) -> NSMutableURLRequest {
		guard let params = self.getQueryParams() else {
			guard let bodyObject = self.getJsonBodyObject() else {
				guard let stringContent = self.getStringBodyContent() else {
					return request
				}
				self.setupStreamStringBody(request, stringContent)
				return request
			}
			self.setupStreamJsonBody(request, bodyObject)
			return request
		}
		self.setupStreamBodyParams(request, params)
		return request
	}
	
	fileprivate func generate(request: NSMutableURLRequest, method: RequestMethod) -> NSMutableURLRequest {
		switch method {
		case .get:
			request.httpMethod = "GET"
			return self.generateQueryRequest(request)
		case .post:
			request.httpMethod = "POST"
			return self.generateStreamRequest(request)
		case .put:
			request.httpMethod = "PUT"
			return self.generateStreamRequest(request)
		case .delete:
			request.httpMethod = "DELETE"
			return self.generateStreamRequest(request)
		}
	}
	
	fileprivate func generateRequest(url: URL, method: RequestMethod) -> URLRequest {
		let request = NSMutableURLRequest(url: url)
		guard let headers = self.getHeaderFields() else {
			return self.generate(request: request, method: method) as URLRequest
		}
		self.setupHeaders(request, headers)
		return self.generate(request: request, method: method) as URLRequest
	}
	
	fileprivate func startSession(forRequest request: URLRequest) {
		self.sessionTask = self.session.dataTask(with: request) { [weak self] (data, response, error) in
			self?.processDataTask(data, response, error)
		}
		self.sessionTask.resume()
	}
	
	fileprivate func doGet(urlString: String) {
		guard let url = URL(string: urlString) else { return }
		let request = self.generateRequest(url: url, method: .get)
		self.startSession(forRequest: request)
	}
	
	fileprivate func doPost(urlString: String) {
		guard let url = URL(string: urlString) else { return }
		let request = self.generateRequest(url: url, method: .post)
		self.startSession(forRequest: request)
	}
	
	fileprivate func doPut(urlString: String) {
		guard let url = URL(string: urlString) else { return }
		let request = self.generateRequest(url: url, method: .put)
		self.startSession(forRequest: request)
	}
	
	fileprivate func doDelete(urlString: String) {
		guard let url = URL(string: urlString) else { return }
		let request = self.generateRequest(url: url, method: .delete)
		self.startSession(forRequest: request)
	}
	
	fileprivate func processDataTask(_ responseData: Data?, _ urlResponse: URLResponse?, _ responseError: Error?) {
		let result = RequestResult()
		result.data = responseData
		result.response = urlResponse as? HTTPURLResponse
		result.error = responseError
		self.requestDidFinish(withResult: result)
	}
	
	fileprivate func requestDidFinish(withResult result: RequestResult) {
		self.isRunning = false
		self.sessionTask = nil
		
		if result.error == nil {
			if result.response != nil && result.response!.statusCode > 299 {
				self.dispatchError(.statusCode(code: result.response!.statusCode, data: result.data))
				return
			}
			
			guard let data = result.data else {
				self.dispatchError(.noResponseData)
				return
			}
			
			self.dispatchSuccess(result.response!.statusCode, data)
		} else {
			self.dispatchError(.connection(error: result.error!))
		}
	}
	
	fileprivate func dispatchError(_ error: BaseRequestError) {
		self.requestDidFinish(withError: error)
	}
	
	fileprivate func dispatchSuccess(_ statusCode: Int, _ data: Data) {
		self.requestDidFinish(withStatusCode: statusCode, andData: data)
	}
	
	public func getBaseErrorString(_ error: BaseRequestError) -> String {
		switch error {
		case .statusCode(let code, let data):
			let dataString = data != nil ? String(data: data!, encoding: .utf8) : nil
			if dataString == nil {
				return "status code \(code)"
			}
			return "status code \(code), response: \(dataString!)"
		default:
			return "\(error)"
		}
	}
	
	open func requestDidFinish(withStatusCode statusCode: Int, andData data: Data) {
		
	}
	
	open func requestDidFinish(withError error: BaseRequestError) {
		
	}
}

internal class RequestResult : NSObject {
	fileprivate(set) var data: Data?
	fileprivate(set) var response: HTTPURLResponse?
	fileprivate(set) var error: Error?
}

fileprivate enum RequestMethod {
	case get, post, put, delete
}

public extension BaseRequest {
	func get(urlString: String) {
		if !self.isRunning {
			self.isRunning = true
			self.doGet(urlString: urlString)
		}
	}
	
	func post(urlString: String) {
		if !self.isRunning {
			self.isRunning = true
			self.doPost(urlString: urlString)
		}
	}
	
	func put(urlString: String) {
		if !self.isRunning {
			self.isRunning = false
			self.doPut(urlString: urlString)
		}
	}
	
	func delete(urlString: String) {
		if !self.isRunning {
			self.isRunning = true
			self.doDelete(urlString: urlString)
		}
	}
	
	func cancel() {
		if self.isRunning && self.sessionTask != nil {
			self.sessionTask.cancel()
			self.sessionTask = nil
		}
	}
}
