//
//  FieldValidator.swift
//  Pods
//
//  Created by Anderson Lucas C. Ramos on 29/03/17.
//
//

import UIKit

public class FieldValidator {
	fileprivate class func strip(chars: [Character], fromString string: String) -> String {
		var output = string
		for char in chars {
			output = output.replacingOccurrences(of: String(char), with: "")
		}
		return output
	}
	
	public class func validate(cpf cpfString: String?, isEmptyTextValid emptyValid: Bool = false) -> Bool {
		guard let text = cpfString else { return emptyValid }
		let strippedText = self.strip(chars: [".", "-", " "], fromString: text)
		return strippedText.lengthOfBytes(using: .utf8) == 11 || (strippedText.isEmpty && emptyValid)
	}
	
	public class func validate(cep cepString: String?, isEmptyTextValid emptyValid: Bool = false) -> Bool {
		guard let text = cepString else { return emptyValid }
		let strippedText = self.strip(chars: [".", "-", " "], fromString: text)
		return strippedText.lengthOfBytes(using: .utf8) == 8 || (strippedText.isEmpty && emptyValid)
	}
	
	public class func validate(date dateString: String?, dateFormat format: String, isEmptyTextValid emptyValid: Bool = false) -> Bool {
		guard let text = dateString else { return emptyValid }
		let chars: [Character] = ["/", ".", "-", " "]
		let strippedText = self.strip(chars: chars, fromString: text)
		let strippedFormat = self.strip(chars: chars, fromString: format)
		return strippedText.lengthOfBytes(using: .utf8) == strippedFormat.lengthOfBytes(using: .utf8) || (text.isEmpty && emptyValid)
	}
	
	public class func validate(email emailString: String?, isEmptyTextValid emptyValid: Bool = false) -> Bool {
		guard let text = emailString else { return emptyValid }
		let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
		let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
		return emailPredicate.evaluate(with: text) || (text.isEmpty && emptyValid)
	}
}
