#
# Be sure to run `pod lib lint RadixLib.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'RadixLib'
  s.version          = '1.0.0'
  s.summary          = 'Biblioteca com códigos reutilizáveis para projetos da Radix.'

#  s.description      = <<-DESC

#                       DESC

  s.homepage              = 'http://service.radixeng.com.br:8888/mobile/libs_ios'
  s.license               = { :type => 'MIT', :file => 'LICENSE' }
  s.author                = { 'Anderson Lucas C. Ramos' => 'anderson.ramos@radixeng.com.br' }
  s.source                = { :git => 'http://service.radixeng.com.br:8888/mobile/libs_ios.git', :tag => s.version.to_s }

  s.ios.deployment_target = '9.0'

  s.source_files = 'RadixLib/Classes/**/*'

  s.dependency 'KeychainAccess', '~> 3.0.2'
  s.dependency 'Swift.Json', '~> 1.2.0'

end
