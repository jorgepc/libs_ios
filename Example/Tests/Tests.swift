import UIKit
import XCTest
import RadixLib
import Swift_Json

class Tests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testDaoTypes() {
		let value1: Int = 10
		
        let userDefaults = DataStorage(withDaoType: DaoUserDefaults.self)
		userDefaults["value1"] = value1
		assert(userDefaults["value1"] as? Int == value1)
		
		// Cannot test the keychain 'cause it needs a entitlements to work
//		let keychain = DataStorage(withDaoType: DaoKeychain.self)
//		keychain["value1"] = value1
//		assert(Int(keychain["value1"] as! String) == value1)
    }
	
	func testLinkedList() {
		let linkedList = LinkedList<String>()
		
		let val1 = "First"
		let node1 = LinkedListNode<String>(value: val1)
		linkedList.append(node1)
		linkedList.append(LinkedListNode<String>(value: "Second"))
		linkedList.append(LinkedListNode<String>(value: "Third"))
		
		assert(linkedList.node(forValue: val1) == node1)
		assert(linkedList[val1] == node1)
		
		guard let node = linkedList.first else {
			assertionFailure()
			return
		}
		assert(node.value == "First")
		assert(node.next?.value == "Second")
		assert(node.next?.previous?.value == "First")
		assert(node.next?.next?.value == "Third")
	}
	
	func testValidators() {
		let cpf = "131.412.007-77"
		let cep = "20.550-050"
		let date = "30/03/1989"
		let email = "andluclive@gtmail.com"
		
		assert(FieldValidator.validate(cpf: cpf))
		assert(FieldValidator.validate(cep: cep))
		assert(FieldValidator.validate(date: date, dateFormat: "dd/MM/yyyy"))
		assert(FieldValidator.validate(email: email))
	}
}
