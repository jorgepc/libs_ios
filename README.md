# RadixLib

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.
To use the DaoKeychain class you need to add the keychain-access-groups entitlements. 

## Requirements

## Installation

RadixLib is available through the private gitlab of Radix. To install it, simply add the following line to your Podfile:

```ruby
pod "RadixLib", :git => 'http://service.radixeng.com.br:8888/mobile/libs_ios.git', :branch => 'develop'
```

And then run the `pod install` command in the terminal.

After the installation completes, add the following lines to your target in the Podfile of your project:

```ruby
post_install do |installer|
	 installer.pods_project.build_configurations.each do |config|
		 if config.name == 'Debug'
			 config.build_settings['SWIFT_ACTIVE_COMPILATION_CONDITIONS'] = '$(inherited) DEBUG'
		 end
	 end
     installer.pods_project.targets.each do |target|
		if target.name == 'RadixLib'
			target.build_configurations.each do |config|
				if config.name == 'Debug'
					config.build_settings['SWIFT_ACTIVE_COMPILATION_CONDITIONS'] = '$(inherited) DEBUG'
				end
			end
		end
     end
  end
```

Obs: Currently the RadixLib is in development, there are no releases available.

## Author

Anderson Lucas C. Ramos, anderson.ramos@radixeng.com.br

## License

RadixLib is available under the MIT license. See the LICENSE file for more info.
